const app = require('../server.js');
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

let should = chai.should();

describe('/GET ', () => {
    it('it should GET', (done) => {
        chai.request(app)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.message.should.be.a('string');
                done();
            });
    });
});
